# Responsive Web Design Projects

Code to submit for freeCodeCamp certification.

Each project sources can be found on the corresponding folder in "dist".

Website can be visited [here](https://mam33.gitlab.io/fcc-responsive-web-design/).

Certification can be found [here](https://www.freecodecamp.org/certification/fcc1725526a-8dab-4643-be7b-c6bd5ad2a769/responsive-web-design).
